from logging import currentframe
from flask import (
    abort,
    Blueprint,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask.globals import current_app
from flask.views import MethodView
from models.user import UserModel
from flask_login import LoginManager, login_user, logout_user
from werkzeug.security import check_password_hash
from urllib.parse import urlparse, urljoin
import requests
from oauthlib.oauth2 import WebApplicationClient
import json

bp = Blueprint("auth", __name__, url_prefix="/auth")
login_manager = LoginManager()
login_manager.login_view = "auth.login"
login_manager.login_message = u"Kérlek, jelentkezz be!"
login_manager.login_message_category = "info"


@login_manager.user_loader
def user_loader(username):
    return UserModel.find_by_username(username)


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


class LoginView(MethodView):
    def get(self):
        return render_template("auth/login.html")

    def post(self):
        username = request.form["username"]
        password = request.form["password"]
        user = UserModel.find_by_username(username)

        if user is None or not check_password_hash(user.password_hash, password):
            flash("Rossz felhasználónév vagy jelszó.", "error")
            return render_template("auth/login.html")
        login_user(user)

        next = request.args.get("next")
        if not is_safe_url(next):
            return abort(400)

        return redirect(next or url_for("ulti.games"))


def get_google_provider_cfg():
    return requests.get(current_app.config["GOOGLE_DISCOVERY_URL"]).json()


class LoginWithGoogleView(MethodView):
    def get(self):
        google_provider_cfg = get_google_provider_cfg()
        authorization_endpoint = google_provider_cfg["authorization_endpoint"]

        client = WebApplicationClient(current_app.config["GOOGLE_CLIENT_ID"])
        request_uri = client.prepare_request_uri(
            authorization_endpoint,
            redirect_uri=request.base_url + "/callback",
            scope=["email"],
        )
        return redirect(request_uri)


class GoogleCallbackView(MethodView):
    def get(self):
        code = request.args.get("code")
        client = WebApplicationClient(current_app.config["GOOGLE_CLIENT_ID"])
        google_provider_cfg = get_google_provider_cfg()
        token_endpoint = google_provider_cfg["token_endpoint"]
        token_url, headers, body = client.prepare_token_request(
            token_endpoint,
            authorization_response=request.url,
            redirect_url=request.base_url,
            code=code,
        )
        token_response = requests.post(
            token_url,
            headers=headers,
            data=body,
            auth=(
                current_app.config["GOOGLE_CLIENT_ID"],
                current_app.config["GOOGLE_CLIENT_SECRET"],
            ),
        )

        client.parse_request_body_response(json.dumps(token_response.json()))

        userinfo_endpoint = google_provider_cfg["userinfo_endpoint"]
        uri, headers, body = client.add_token(userinfo_endpoint)
        userinfo_response = requests.get(uri, headers=headers, data=body)

        if userinfo_response.json().get("email_verified"):
            users_email = userinfo_response.json()["email"]
        else:
            flash("Hiba történt a Google azonosítás során.", "error")
            return render_template("auth/login.html")

        user = UserModel.find_by_username(users_email)
        if not user:
            flash("Az email címed nincs regisztrálva az alkalmazásban.", "error")
            return render_template("auth/login.html")
        login_user(user)
        return redirect(url_for("ulti.games"))


class LogoutView(MethodView):
    def get(self):
        logout_user()
        return redirect(url_for("auth.login"))
