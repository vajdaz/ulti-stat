FROM ubuntu:20.04

RUN apt-get update -y && \
    apt-get install -y python3 python3-pip netcat

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

COPY ./app /app

ENV FLASK_APP=app FLASK_ENV=release ULTI_DEFAULT_ADMIN_PWD=admin

CMD [ "flask", "run", "--host=0.0.0.0"]
