from db import db
from flask_login import UserMixin
from typing import Dict, Union
from werkzeug.security import generate_password_hash

UserJSON = Dict[str, Union[int, str]]


class UserModel(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200))
    password_hash = db.Column(db.String(200))

    def __init__(self, username: str, password: str):
        self.username = username
        self.password_hash = generate_password_hash(password, "sha256")

    @classmethod
    def find_by_username(cls, username: str) -> "UserModel":
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_id(cls, _id: int) -> "UserModel":
        return cls.query.filter_by(id=_id).first()

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def json(self) -> UserJSON:
        return {"id": self.id, "username": self.username}

    # Method for Flask-Login
    def get_id(self):
        return self.username
