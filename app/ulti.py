import datetime
from flask import (
    Blueprint,
    render_template,
    request,
    url_for,
    jsonify,
)
from flask.views import MethodView
from flask_login import login_required
from models.player import PlayerModel
from models.game import GameModel, Role


bp = Blueprint("ulti", __name__, url_prefix="/")


class GamesListView(MethodView):
    methods = ["GET"]

    @login_required
    def get(self):
        page = request.args.get("page", 1, type=int)
        games = GameModel.find_all_page(page)
        next_url = (
            url_for("ulti.games", page=games.next_num) if games.has_next else None
        )
        prev_url = (
            url_for("ulti.games", page=games.prev_num) if games.has_prev else None
        )
        page_from = max([1, games.page - 2])
        page_to = min([games.page + 2, games.pages])
        page_from = max([1, page_to - 4])
        page_to = min([page_from + 4, games.pages]) + 1
        return render_template(
            "ulti/games.html",
            games=games.items,
            players=PlayerModel.find_all(),
            next_url=next_url,
            prev_url=prev_url,
            page_current=games.page,
            page_range=range(page_from, page_to),
            Role=Role,
        )


class ResultData:
    def __init__(self):
        self.score = 0
        self.score_per_game = 0
        self.no_of_games = 0
        self.no_of_played_games = 0
        self.as_challenger = 0
        self.no_of_played_games_as_challenger = 0


class ResultSummaryView(MethodView):
    methods = ["GET"]

    @login_required
    def get(self):
        resultsPerName = {}
        date_from_req = request.args.get("date_from")
        date_to_req = request.args.get("date_to")

        date_from = None
        date_to = None

        if not date_from_req:
            date_from = datetime.date.today()
        if not date_to_req:
            date_to = datetime.date.today()

        try:
            if not date_from:
                date_from = datetime.date.fromisoformat(date_from_req)
            if not date_to:
                date_to = datetime.date.fromisoformat(date_to_req)
        except ValueError:
            return render_template(
                "ulti/result-summary.html",
                results=enumerate(resultsPerName),
                no_of_results=len(resultsPerName),
                message="Érvénytelen dátum",
            )

        # date_to is meant to be inclusive. If you sepcify 2021-03-14, the datetime object
        # will be 2021-03-14 0:00, but you want to see all games for 2021-03-14, too. Therefore
        # we add one day, so the query will carch those games.
        date_to_corrected = date_to + datetime.timedelta(days=1)
        games = GameModel.find_from_to(date_from, date_to_corrected)

        for game in games:
            for role in Role:
                name = game.nameForRole(role)
                data = resultsPerName.get(name, ResultData())
                data.score += game.value(role)
                data.no_of_games += 1
                data.no_of_played_games += 0 if game.throwIn else 1
                if role == Role.challenger:
                    data.as_challenger += 1
                    data.no_of_played_games_as_challenger += 0 if game.throwIn else 1
                resultsPerName[name] = data

        results = [
            {"name": name, "data": data} for name, data in resultsPerName.items()
        ]

        for result in results:
            result["data"].score_per_game = round(
                float(result["data"].score) / float(result["data"].no_of_games), 2
            )

        results.sort(key=lambda x: x["data"].score_per_game, reverse=True)

        return render_template(
            "ulti/result-summary.html",
            results=results,
            date_from=date_from,
            date_to=date_to,
        )


class GameApi(MethodView):
    methods = ["GET", "POST", "PUT", "DELETE"]

    @login_required
    def get(self, id):
        game = GameModel.find_by_id(id)
        if not game:
            return jsonify({"message": "Nem találtam a játszmát."}), 404
        return jsonify(game)

    @login_required
    def post(self, id):
        game = GameModel(None, None, None)
        game.update_from(request.form)
        try:
            game.save_to_db()
        except Exception as e:
            return (
                jsonify({"message": "Nem sikerült létrehozni a játszmát."}),
                500,
            )
        return jsonify({"message": "A játszma létrehozása sikeres volt."})

    @login_required
    def put(self, id):
        game = GameModel.find_by_id(id)
        if not game:
            return jsonify({"message": "Nem találtam a játszmát."}), 404
        else:
            game.update_from(request.form)
        try:
            game.save_to_db()
        except Exception as e:
            return (
                jsonify({"message": "Nem sikerült frissíteni a játszmát."}),
                500,
            )
        return jsonify({"message": "A játszma frissítése sikeres volt."})

    @login_required
    def delete(self, id):
        game = GameModel.find_by_id(id)
        if not game:
            return jsonify({"message": "Nem találtam a játszmát."}), 404
        try:
            game.remove_from_db()
        except Exception as e:
            return (
                jsonify({"message": "Nem sikerült törölni a játszmát."}),
                500,
            )
        return jsonify({"message": "A játszma eltávolítása sikeres volt."})


class PlayerListView(MethodView):
    methods = ["GET"]

    @login_required
    def get(self):
        return render_template("ulti/players.html", players=PlayerModel.find_all())


class PlayerApi(MethodView):
    methods = ["POST", "PUT", "DELETE"]

    @login_required
    def post(self, id):
        player = PlayerModel(request.form["name"])
        try:
            player.save_to_db()
        except Exception as e:
            return (
                jsonify({"message": "Nem sikerült létrehozni a játékost."}),
                500,
            )
        return jsonify({"message": "A játékos létrehozása sikeres volt."})

    @login_required
    def put(self, id):
        player = PlayerModel.find_by_id(id)
        if not player:
            return jsonify({"message": "Nem találtam a játékost."}), 404
        else:
            player.name = request.form["name"]
        try:
            player.save_to_db()
        except Exception as e:
            return (
                jsonify({"message": "Nem sikerült frissíteni a játékost."}),
                500,
            )

        return jsonify({"message": "A játékos frissítése sikeres volt."})

    @login_required
    def delete(self, id):
        player = PlayerModel.find_by_id(id)
        if not player:
            return jsonify({"message": "Nem találtam a játékost."}), 404
        try:
            player.remove_from_db()
        except Exception as e:
            return (
                jsonify(
                    {
                        "message": "Nem sikerült törölni a játékost. Talán mert részt vesz egy játszmában."
                    }
                ),
                500,
            )
        return jsonify({"message": "A játékos eltávolítása sikeres volt."})


class LastPlayersApi(MethodView):
    def get(self):
        last_game = GameModel.find_last()
        if not last_game:
            return jsonify({"message": "Nincs egy játék sem"}), 404
        return jsonify(
            {
                "challenger": last_game.challenger.id if last_game.challenger else 0,
                "defender1": last_game.defender1.id if last_game.defender1 else 0,
                "defender2": last_game.defender2.id if last_game.defender2 else 0,
            }
        )
