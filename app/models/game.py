from db import db
from typing import Dict, Union, List
from enum import Enum
import datetime
from models.player import PlayerModel

GameJSON = Dict[str, Union[int, str, bool]]


class ContraLevels(Enum):
    none = 0
    contra = 1
    recontra = 2


class Role(Enum):
    challenger = 0
    defender1 = 1
    defender2 = 2


def current_time_min_precision():
    now = datetime.datetime.now().time()
    return datetime.time(hour=now.hour, minute=now.minute)


def contraFactor(
    role: Role, contraLevel: List[ContraLevels], game_value: int, contra_value: int
):
    if role == Role.challenger:
        raise ValueError()
    if role == Role.defender1 or len(contraLevel) == 1:
        if contraLevel[0] == ContraLevels.contra:
            return game_value + contra_value
        elif contraLevel[0] == ContraLevels.recontra:
            return game_value + contra_value * 3
    else:
        if contraLevel[1] == ContraLevels.contra:
            return game_value + contra_value
        elif contraLevel[1] == ContraLevels.recontra:
            return game_value + contra_value * 3
    return game_value


def computeValue(
    role: Role,
    gamePlayed: bool,
    contraLevel: List[ContraLevels],
    success: bool,
    game_value: int,
    contra_value: int,
) -> int:
    if gamePlayed:
        return contraFactor(
            role,
            contraLevel,
            game_value if success else -game_value,
            contra_value if success else -contra_value,
        )
    return 0


class GameModel(db.Model):
    __tablename__ = "games"

    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)

    date = db.Column(db.Date(), default=datetime.date.today)
    time = db.Column(db.Time(), default=current_time_min_precision)

    challenger_id = db.Column(db.Integer(), db.ForeignKey("players.id"))
    challenger = db.relationship(
        "models.player.PlayerModel", foreign_keys=[challenger_id]
    )
    defender1_id = db.Column(db.Integer(), db.ForeignKey("players.id"))
    defender1 = db.relationship(
        "models.player.PlayerModel", foreign_keys=[defender1_id]
    )
    defender2_id = db.Column(db.Integer(), db.ForeignKey("players.id"))
    defender2 = db.relationship(
        "models.player.PlayerModel", foreign_keys=[defender2_id]
    )

    simple = db.Column(db.Boolean(), default=False)
    fourtyHundred = db.Column(db.Boolean(), default=False)
    fourAsses = db.Column(db.Boolean(), default=False)
    ulti = db.Column(db.Boolean(), default=False)
    betli = db.Column(db.Boolean(), default=False)
    durchmars = db.Column(db.Boolean(), default=False)
    twentyHundred = db.Column(db.Boolean(), default=False)
    throwIn = db.Column(db.Boolean(), default=False)

    redGame = db.Column(db.Boolean(), default=False)
    colorless = db.Column(db.Boolean(), default=False)
    openGame = db.Column(db.Boolean(), default=False)

    simpleSucceed = db.Column(db.Boolean(), default=False)
    fourtyHundredSucceed = db.Column(db.Boolean(), default=False)
    fourAssesSucceed = db.Column(db.Boolean(), default=False)
    ultiSucceed = db.Column(db.Boolean(), default=False)
    betliSucceed = db.Column(db.Boolean(), default=False)
    durchmarsSucceed = db.Column(db.Boolean(), default=False)
    twentyHundredSucceed = db.Column(db.Boolean(), default=False)

    simpleContraLevel = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    fourtyHundredContraLevel = db.Column(
        db.Enum(ContraLevels), default=ContraLevels.none
    )
    fourAssesContraLevel = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    ultiContraLevel = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    betliContraLevel = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    betliContraLevel2 = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    durchmarsContraLevel = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    durchmarsContraLevel2 = db.Column(db.Enum(ContraLevels), default=ContraLevels.none)
    twentyHundredContraLevel = db.Column(
        db.Enum(ContraLevels), default=ContraLevels.none
    )

    renonceByChallenger = db.Column(db.Boolean(), default=False)
    renonceByDefender1 = db.Column(db.Boolean(), default=False)
    renonceByDefender2 = db.Column(db.Boolean(), default=False)
    renonceByPartner = db.Column(db.Boolean(), default=False)

    quietHundredByChallenger = db.Column(db.Boolean(), default=False)
    quietHundredByDefenders = db.Column(db.Boolean(), default=False)
    quietFourAssesByChallenger = db.Column(db.Boolean(), default=False)
    quietFourAssesByDefenders = db.Column(db.Boolean(), default=False)
    quietUltiByChallenger = db.Column(db.Boolean(), default=False)
    quietUltiByDefender1 = db.Column(db.Boolean(), default=False)
    quietUltiByDefender2 = db.Column(db.Boolean(), default=False)
    quietUltiFailed = db.Column(db.Boolean(), default=False)

    def __init__(self, challenger, defender1, defender2):
        self.challenger = challenger
        self.defender1 = defender1
        self.defender2 = defender2
        self.reset()

    def reset(self):
        self.simple = False
        self.fourtyHundred = False
        self.fourAsses = False
        self.ulti = False
        self.betli = False
        self.durchmars = False
        self.twentyHundred = False
        self.throwIn = False
        self.redGame = False
        self.colorless = False
        self.openGame = False
        self.simpleSucceed = False
        self.fourtyHundredSucceed = False
        self.fourAssesSucceed = False
        self.ultiSucceed = False
        self.betliSucceed = False
        self.durchmarsSucceed = False
        self.twentyHundredSucceed = False
        self.simpleContraLevel = ContraLevels.none
        self.fourtyHundredContraLevel = ContraLevels.none
        self.fourAssesContraLevel = ContraLevels.none
        self.ultiContraLevel = ContraLevels.none
        self.betliContraLevel = ContraLevels.none
        self.betliContraLevel2 = ContraLevels.none
        self.durchmarsContraLevel = ContraLevels.none
        self.durchmarsContraLevel2 = ContraLevels.none
        self.twentyHundredContraLevel = ContraLevels.none
        self.renonceByChallenger = False
        self.renonceByDefender1 = False
        self.renonceByDefender2 = False
        self.renonceByPartner = False
        self.quietHundredByChallenger = False
        self.quietHundredByDefenders = False
        self.quietFourAssesByChallenger = False
        self.quietFourAssesByDefenders = False
        self.quietUltiByChallenger = False
        self.quietUltiByDefender1 = False
        self.quietUltiByDefender2 = False
        self.quietUltiFailed = False

    @classmethod
    def find_by_id(cls, id: int) -> "GameModel":
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_datetime(cls, date: datetime.date, time: datetime.time) -> "GameModel":
        return cls.query.filter_by(date=date, time=time).first()

    @classmethod
    def find_all(cls) -> "List[GamerModel]":
        return cls.query.order_by(cls.date.desc(), cls.time.desc()).all()

    @classmethod
    def find_last(cls) -> "GamerModel":
        return cls.query.order_by(cls.date.desc(), cls.time.desc()).first()

    @classmethod
    def find_all_page(cls, page: int) -> "List[GamerModel]":
        return cls.query.order_by(cls.date.desc(), cls.time.desc()).paginate(
            page, 10, False
        )

    @classmethod
    def find_from_to(cls, date_from, date_to) -> "List[GamerModel]":
        return cls.query.filter(cls.date.between(date_from, date_to))

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def remove_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def designation(self):
        designation = []
        if self.throwIn:
            designation.append("bedobott")
        if self.redGame and not self.colorless:
            designation.append("piros")
        if self.fourtyHundred:
            designation.append("40-100")
        if self.twentyHundred:
            designation.append("20-100")
        if self.ulti:
            designation.append("ulti")
        if self.fourAsses:
            designation.append("négy ász")
        if self.colorless and self.durchmars:
            designation.append("színtelen")
        if self.openGame:
            designation.append("terített")
        if self.betli:
            if self.redGame and self.colorless:
                designation.append("rebetli")
            else:
                designation.append("betli")
        if self.durchmars:
            if self.redGame and self.colorless:
                designation.append("redurchmars")
            else:
                designation.append("durchmars")
        if len(designation) == 1 and designation[0] == "piros":
            designation.append("passz")
        elif len(designation) == 0 or (
            len(designation) == 1 and designation[0] == "bedobott"
        ):
            designation.append("passz")
        return " ".join(designation)

    def value(self, role: Role) -> int:
        # There was no renonce, use the usual calculation
        if (
            not (
                self.renonceByChallenger
                or self.renonceByDefender1
                or self.renonceByDefender2
            )
            or self.renonceByChallenger
        ):
            return self.valueWithoutRenonce(role)

        if not self.renonceByPartner:
            # There was a renonce and the renonce caller was not the partner
            # -> Challenger called the renonce, or the renonce was made by him.
            # In both cases the challenger receives/pays the usual sum.
            if role == Role.challenger:
                # Renonced games are always "failed" games. A failed ulti counts
                # doubled. However if one of the defenders renonced, the
                # challenger gets the normal success price of the game. We need
                # a little trick here, and assume that the game succeeded.
                if not self.renonceByChallenger and self.ulti:
                    return self.valueWithoutRenonce(role, True)
                else:
                    return -self.valueWithoutRenonce(role)
            # In case of the defenders there are two alternatives:
            # * Renonce by self -> pay the value of both other roles
            # * Renonce by partner -> receive the normal payment
            elif role == Role.defender1:
                return (
                    self.valueWithoutRenonce(role)
                    if self.renonceByDefender2
                    else (
                        -self.valueWithoutRenonce(Role.challenger, True)
                        - self.valueWithoutRenonce(Role.defender2)
                    )
                )
            else:  # role == Role.defender2
                return (
                    self.valueWithoutRenonce(role)
                    if self.renonceByDefender1
                    else -self.valueWithoutRenonce(Role.challenger, True)
                    - self.valueWithoutRenonce(Role.defender1)
                )

        # If we came as far as this, one of the defenders called renonce
        # against his partner.
        # The challenger receives the usual win
        if role == Role.challenger:
            return -self.valueWithoutRenonce(role)

        # In case of the defenders there are two alternatives:
        # * Renonce by self -> pay the value only for challenger
        # * Renonce by partner -> receive nothing
        elif role == Role.defender1:
            return (
                0
                if self.renonceByDefender2
                else self.valueWithoutRenonce(Role.challenger)
            )
        else:
            return (
                0
                if self.renonceByDefender1
                else self.valueWithoutRenonce(Role.challenger)
            )

        return 0

    def valueWithoutRenonce(self, role: Role, assumeSuccess=False) -> int:

        # The value of the game may differ for the two defenders. The value for
        # the challenger is the sum of the values from the two defenders
        if role == Role.challenger:
            return -(
                self.valueWithoutRenonce(Role.defender1, assumeSuccess)
                + self.valueWithoutRenonce(Role.defender2, assumeSuccess)
            )

        v = 0
        if (
            not (self.quietHundredByChallenger or self.quietHundredByDefenders)
            or self.simpleContraLevel != ContraLevels.none
        ):
            v += computeValue(
                role,
                self.simple,
                [self.simpleContraLevel],
                self.simpleSucceed or assumeSuccess,
                1,
                1,
            )
        v += computeValue(
            role,
            self.fourtyHundred,
            [self.fourtyHundredContraLevel],
            self.fourtyHundredSucceed or assumeSuccess,
            4,
            4,
        )
        v += computeValue(
            role,
            self.fourAsses,
            [self.fourAssesContraLevel],
            self.fourAssesSucceed or assumeSuccess,
            4,
            4,
        )
        # Special case: failed ulti has double value
        v += computeValue(
            role,
            self.ulti,
            [self.ultiContraLevel],
            self.ultiSucceed or assumeSuccess,
            4 if self.ultiSucceed or assumeSuccess else 8,
            4,
        )
        v += (
            computeValue(
                role,
                self.betli,
                [self.betliContraLevel, self.betliContraLevel2],
                self.betliSucceed or assumeSuccess,
                5,
                5,
            )
            * (4 if self.openGame else 1)
        )
        # Special case: different contra levels for colorless durchmars, but
        # common contra levels for colorful durchmars
        v += (
            computeValue(
                role,
                self.durchmars,
                [self.durchmarsContraLevel, self.durchmarsContraLevel2]
                if self.colorless
                else [self.durchmarsContraLevel],
                self.durchmarsSucceed or assumeSuccess,
                6,
                6,
            )
            * (2 if self.openGame else 1)
        )
        v += computeValue(
            role,
            self.twentyHundred,
            [self.twentyHundredContraLevel],
            self.twentyHundredSucceed or assumeSuccess,
            8,
            8,
        )

        if self.quietHundredByChallenger:
            v += 2
        if self.quietHundredByDefenders:
            v += -2

        if self.quietFourAssesByChallenger:
            v += 2
        if self.quietFourAssesByDefenders:
            v += -2

        if (
            self.quietUltiByChallenger
            or self.quietUltiByDefender1
            or self.quietUltiByDefender2
        ):
            q = -4 if self.quietUltiFailed else 2
            v += (
                -2 * q
                if (role == Role.defender1 and self.quietUltiByDefender1)
                or (role == Role.defender2 and self.quietUltiByDefender2)
                else q
            )

        if self.redGame:
            v *= 2

        return -v

    def nameForRole(self, role: Role):
        if role == Role.challenger:
            return self.challenger.name if self.challenger else "<ismeretlen játékos>"
        elif role == Role.defender1:
            return self.defender1.name if self.defender1 else "<ismeretlen játékos>"
        else:
            return self.defender2.name if self.defender2 else "<ismeretlen játékos>"

    def json(self) -> GameJSON:
        return {
            "id": self.id,
            "date": self.date.isoformat(),
            "time": self.time.isoformat(),
            "challenger": self.challenger_id,
            "defender1": self.defender1_id,
            "defender2": self.defender2_id,
            "plain-game": self.simple,
            "fourty-hundred": self.fourtyHundred,
            "four-asses": self.fourAsses,
            "ulti": self.ulti,
            "betli": self.betli,
            "durchmars": self.durchmars,
            "twenty-hundred": self.twentyHundred,
            "throw-in": self.throwIn,
            "red-game": self.redGame,
            "colorless": self.colorless,
            "open-game": self.openGame,
            "plain-game-success": self.simpleSucceed,
            "fourty-hundred-success": self.fourtyHundredSucceed,
            "four-asses-success": self.fourAssesSucceed,
            "ulti-success": self.ultiSucceed,
            "betli-success": self.betliSucceed,
            "durchmars-success": self.durchmarsSucceed,
            "twenty-hundred-success": self.twentyHundredSucceed,
            "plain-game-contra": self.simpleContraLevel.value,
            "fourty-hundred-contra": self.fourtyHundredContraLevel.value,
            "four-asses-contra": self.fourAssesContraLevel.value,
            "ulti-contra": self.ultiContraLevel.value,
            "betli-contra": self.betliContraLevel.value,
            "betli-contra2": self.betliContraLevel2.value,
            "durchmars-contra": self.durchmarsContraLevel.value,
            "durchmars-contra2": self.durchmarsContraLevel2.value,
            "twenty-hundred-contra": self.twentyHundredContraLevel.value,
            "renonce-by-partner": self.renonceByPartner,
            "renonce-challenger": self.renonceByChallenger,
            "renonce-defender1": self.renonceByDefender1,
            "renonce-defender2": self.renonceByDefender2,
            "quiet-hundred-challenger": self.quietHundredByChallenger,
            "quiet-hundred-defenders": self.quietHundredByDefenders,
            "quiet-four-asses-challenger": self.quietFourAssesByChallenger,
            "quiet-four-asses-defenders": self.quietFourAssesByDefenders,
            "quiet-ulti-challenger": self.quietUltiByChallenger,
            "quiet-ulti-defender1": self.quietUltiByDefender1,
            "quiet-ulti-defender2": self.quietUltiByDefender2,
            "quiet-ulti-failed": self.quietUltiFailed,
        }

    def get_contra_from_dict(self, dict, key):
        value = dict.get(key)
        if value == None or len(value) == 0:
            return ContraLevels.none
        return ContraLevels(int(value))

    def update_from(self, dict):
        self.date = datetime.datetime.fromisoformat(dict["date"])
        self.time = datetime.time.fromisoformat(dict["time"])

        self.challenger = (
            PlayerModel.find_by_id(int(dict["challenger"]))
            if "challenger" in dict
            else None
        )
        self.defender1 = (
            PlayerModel.find_by_id(int(dict["defender1"]))
            if "defender1" in dict
            else None
        )
        self.defender2 = (
            PlayerModel.find_by_id(int(dict["defender2"]))
            if "defender2" in dict
            else None
        )

        self.simple = dict.get("plain-game") != None
        self.fourtyHundred = dict.get("fourty-hundred") != None
        self.fourAsses = dict.get("four-asses") != None
        self.ulti = dict.get("ulti") != None
        self.betli = dict.get("betli") != None
        self.durchmars = dict.get("durchmars") != None
        self.twentyHundred = dict.get("twenty-hundred") != None
        self.throwIn = dict.get("throw-in") != None
        self.redGame = dict.get("red-game") != None
        self.colorless = dict.get("colorless") != None
        self.openGame = dict.get("open-game") != None
        self.simpleSucceed = dict.get("plain-game-success") != None
        self.fourtyHundredSucceed = dict.get("fourty-hundred-success") != None
        self.fourAssesSucceed = dict.get("four-asses-success") != None
        self.ultiSucceed = dict.get("ulti-success") != None
        self.betliSucceed = dict.get("betli-success") != None
        self.durchmarsSucceed = dict.get("durchmars-success") != None
        self.twentyHundredSucceed = dict.get("twenty-hundred-success") != None

        self.simpleContraLevel = self.get_contra_from_dict(dict, "plain-game-contra")
        self.fourtyHundredContraLevel = self.get_contra_from_dict(
            dict, "fourty-hundred-contra"
        )
        self.fourAssesContraLevel = self.get_contra_from_dict(dict, "four-asses-contra")
        self.ultiContraLevel = self.get_contra_from_dict(dict, "ulti-contra")
        self.betliContraLevel = self.get_contra_from_dict(dict, "betli-contra")
        self.betliContraLevel2 = self.get_contra_from_dict(dict, "betli-contra2")
        self.durchmarsContraLevel = self.get_contra_from_dict(dict, "durchmars-contra")
        self.durchmarsContraLevel2 = self.get_contra_from_dict(
            dict, "durchmars-contra2"
        )
        self.twentyHundredContraLevel = self.get_contra_from_dict(
            dict, "twenty-hundred-contra"
        )

        self.renonceByPartner = dict.get("renonce-by-partner") != None
        self.renonceByChallenger = dict.get("renonce-challenger") != None
        self.renonceByDefender1 = dict.get("renonce-defender1") != None
        self.renonceByDefender2 = dict.get("renonce-defender2") != None
        self.quietHundredByChallenger = dict.get("quiet-hundred-challenger") != None
        self.quietHundredByDefenders = dict.get("quiet-hundred-defenders") != None
        self.quietFourAssesByChallenger = (
            dict.get("quiet-four-asses-challenger") != None
        )
        self.quietFourAssesByDefenders = dict.get("quiet-four-asses-defenders") != None
        self.quietUltiByChallenger = dict.get("quiet-ulti-challenger") != None
        self.quietUltiByDefender1 = dict.get("quiet-ulti-defender1") != None
        self.quietUltiByDefender2 = dict.get("quiet-ulti-defender2") != None
        self.quietUltiFailed = dict.get("quiet-ulti-failed") != None
