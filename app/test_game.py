import unittest

from models.player import PlayerModel
from models.game import ContraLevels, GameModel, Role


class TestJson(unittest.TestCase):
    def test_designations(self):
        dummy = GameModel(
            PlayerModel("player1"), PlayerModel("player2"), PlayerModel("player3")
        )

        dummy.reset()
        dummy.simple = True
        dummy.fourAsses = True
        self.assertEqual(dummy.designation(), "négy ász")
        dummy.redGame = True
        self.assertEqual(dummy.designation(), "piros négy ász")

        dummy.reset()
        dummy.simple = True
        self.assertEqual(dummy.designation(), "passz")
        dummy.throwIn = True
        self.assertEqual(dummy.designation(), "bedobott passz")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "bedobott ulti")
        dummy.fourAsses = True
        dummy.throwIn = False
        self.assertEqual(dummy.designation(), "ulti négy ász")
        dummy.simple = False
        dummy.fourtyHundred = True
        self.assertEqual(dummy.designation(), "40-100 ulti négy ász")
        dummy.fourAsses = False
        self.assertEqual(dummy.designation(), "40-100 ulti")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "40-100")
        dummy.fourAsses = True
        self.assertEqual(dummy.designation(), "40-100 négy ász")

        dummy.reset()
        dummy.redGame = True
        dummy.simple = True
        self.assertEqual(dummy.designation(), "piros passz")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "piros ulti")
        dummy.fourAsses = True
        self.assertEqual(dummy.designation(), "piros ulti négy ász")
        dummy.simple = False
        dummy.fourtyHundred = True
        self.assertEqual(dummy.designation(), "piros 40-100 ulti négy ász")
        dummy.fourAsses = False
        self.assertEqual(dummy.designation(), "piros 40-100 ulti")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "piros 40-100")
        dummy.fourAsses = True
        self.assertEqual(dummy.designation(), "piros 40-100 négy ász")

        dummy.reset()
        dummy.ulti = True
        dummy.fourAsses = True
        dummy.twentyHundred = True
        self.assertEqual(dummy.designation(), "20-100 ulti négy ász")
        dummy.fourAsses = False
        self.assertEqual(dummy.designation(), "20-100 ulti")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "20-100")
        dummy.fourAsses = True
        self.assertEqual(dummy.designation(), "20-100 négy ász")

        dummy.reset()
        dummy.redGame = True
        dummy.ulti = True
        dummy.fourAsses = True
        dummy.twentyHundred = True
        self.assertEqual(dummy.designation(), "piros 20-100 ulti négy ász")
        dummy.fourAsses = False
        self.assertEqual(dummy.designation(), "piros 20-100 ulti")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "piros 20-100")
        dummy.fourAsses = True
        self.assertEqual(dummy.designation(), "piros 20-100 négy ász")

        dummy.reset()
        dummy.betli = True
        dummy.colorless = True
        self.assertEqual(dummy.designation(), "betli")
        dummy.redGame = True
        self.assertEqual(dummy.designation(), "rebetli")
        dummy.redGame = False
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "terített betli")

        dummy.reset()
        dummy.durchmars = True
        self.assertEqual(dummy.designation(), "durchmars")
        dummy.colorless = True
        self.assertEqual(dummy.designation(), "színtelen durchmars")
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "színtelen terített durchmars")
        dummy.redGame = True
        dummy.colorless = False
        self.assertEqual(dummy.designation(), "piros terített durchmars")
        dummy.redGame = False
        self.assertEqual(dummy.designation(), "terített durchmars")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "ulti terített durchmars")
        dummy.openGame = False
        self.assertEqual(dummy.designation(), "ulti durchmars")
        dummy.redGame = True
        self.assertEqual(dummy.designation(), "piros ulti durchmars")
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "piros ulti terített durchmars")

        dummy.reset()
        dummy.durchmars = True
        dummy.fourtyHundred = True
        self.assertEqual(dummy.designation(), "40-100 durchmars")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "40-100 ulti durchmars")
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "40-100 ulti terített durchmars")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "40-100 terített durchmars")

        dummy.reset()
        dummy.redGame = True
        dummy.durchmars = True
        self.assertEqual(dummy.designation(), "piros durchmars")
        dummy.colorless = True
        self.assertEqual(dummy.designation(), "színtelen redurchmars")
        dummy.colorless = False
        dummy.fourtyHundred = True
        self.assertEqual(dummy.designation(), "piros 40-100 durchmars")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "piros 40-100 ulti durchmars")
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "piros 40-100 ulti terített durchmars")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "piros 40-100 terített durchmars")

        dummy.reset()
        dummy.durchmars = True
        dummy.twentyHundred = True
        self.assertEqual(dummy.designation(), "20-100 durchmars")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "20-100 ulti durchmars")
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "20-100 ulti terített durchmars")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "20-100 terített durchmars")

        dummy.reset()
        dummy.redGame = True
        dummy.durchmars = True
        dummy.twentyHundred = True
        self.assertEqual(dummy.designation(), "piros 20-100 durchmars")
        dummy.ulti = True
        self.assertEqual(dummy.designation(), "piros 20-100 ulti durchmars")
        dummy.openGame = True
        self.assertEqual(dummy.designation(), "piros 20-100 ulti terített durchmars")
        dummy.ulti = False
        self.assertEqual(dummy.designation(), "piros 20-100 terített durchmars")

    def test_values(self):
        dummy = GameModel(
            PlayerModel("player1"), PlayerModel("player2"), PlayerModel("player3")
        )

        # bedobott jatek
        dummy.reset()
        dummy.simple = True
        dummy.throwIn = True
        dummy.simpleContraLevel = ContraLevels.contra
        self.assertEqual(dummy.value(Role.challenger), -4)
        self.assertEqual(dummy.value(Role.defender1), 2)
        self.assertEqual(dummy.value(Role.defender2), 2)

        # negy asz
        dummy.reset()
        dummy.simple = True
        dummy.fourAsses = True
        dummy.simpleSucceed = True
        dummy.fourAssesSucceed = True
        self.assertEqual(dummy.value(Role.challenger), 10)
        self.assertEqual(dummy.value(Role.defender1), -5)
        self.assertEqual(dummy.value(Role.defender2), -5)
        dummy.redGame = True
        self.assertEqual(dummy.value(Role.challenger), 20)
        self.assertEqual(dummy.value(Role.defender1), -10)
        self.assertEqual(dummy.value(Role.defender2), -10)

        # ulti csendes 100 vedo 1
        dummy.reset()
        dummy.simple = True
        dummy.ulti = True
        dummy.simpleSucceed = True
        dummy.ultiSucceed = True
        dummy.quietHundredByDefenders = True
        self.assertEqual(dummy.value(Role.challenger), 4)
        self.assertEqual(dummy.value(Role.defender1), -2)
        self.assertEqual(dummy.value(Role.defender2), -2)
        dummy.redGame = True
        self.assertEqual(dummy.value(Role.challenger), 8)
        self.assertEqual(dummy.value(Role.defender1), -4)
        self.assertEqual(dummy.value(Role.defender2), -4)

        # ulti csendes 100 felvevo
        dummy.reset()
        dummy.simple = True
        dummy.ulti = True
        dummy.simpleSucceed = True
        dummy.ultiSucceed = True
        dummy.quietHundredByChallenger = True
        self.assertEqual(dummy.value(Role.challenger), 12)
        self.assertEqual(dummy.value(Role.defender1), -6)
        self.assertEqual(dummy.value(Role.defender2), -6)
        dummy.redGame = True
        self.assertEqual(dummy.value(Role.challenger), 24)
        self.assertEqual(dummy.value(Role.defender1), -12)
        self.assertEqual(dummy.value(Role.defender2), -12)

        # bukott ulti, passz ok
        dummy.reset()
        dummy.simple = True
        dummy.ulti = True
        dummy.simpleSucceed = True
        dummy.ultiSucceed = False
        self.assertEqual(dummy.value(Role.challenger), -14)
        self.assertEqual(dummy.value(Role.defender1), 7)
        self.assertEqual(dummy.value(Role.defender2), 7)
        dummy.redGame = True
        self.assertEqual(dummy.value(Role.challenger), -28)
        self.assertEqual(dummy.value(Role.defender1), 14)
        self.assertEqual(dummy.value(Role.defender2), 14)

        # ulti, elso vedo renonszol
        dummy.reset()
        dummy.simple = True
        dummy.ulti = True
        dummy.renonceByDefender1 = True
        self.assertEqual(dummy.value(Role.challenger), 10)
        self.assertEqual(dummy.value(Role.defender1), -19)
        self.assertEqual(dummy.value(Role.defender2), 9)

        # bukott ulti, passz ok, csendes szaz felvevo, kontra ulti
        dummy.reset()
        dummy.simple = True
        dummy.ulti = True
        dummy.simpleSucceed = True
        dummy.ultiSucceed = False
        dummy.ultiContraLevel = ContraLevels.contra
        dummy.quietHundredByChallenger = True
        self.assertEqual(dummy.value(Role.challenger), -20)
        self.assertEqual(dummy.value(Role.defender1), 10)
        self.assertEqual(dummy.value(Role.defender2), 10)
        dummy.redGame = True
        self.assertEqual(dummy.value(Role.challenger), -40)
        self.assertEqual(dummy.value(Role.defender1), 20)
        self.assertEqual(dummy.value(Role.defender2), 20)

        # csendes ulti felvevo siker/bukva
        dummy.reset()
        dummy.simple = True
        dummy.simpleSucceed = True
        dummy.quietUltiByChallenger = True
        self.assertEqual(dummy.value(Role.challenger), 6)
        self.assertEqual(dummy.value(Role.defender1), -3)
        self.assertEqual(dummy.value(Role.defender2), -3)
        dummy.quietUltiFailed = True
        self.assertEqual(dummy.value(Role.challenger), -6)
        self.assertEqual(dummy.value(Role.defender1), 3)
        self.assertEqual(dummy.value(Role.defender2), 3)

        # csendes ulti vedo 1 siker/bukva
        dummy.reset()
        dummy.simple = True
        dummy.simpleSucceed = True
        dummy.quietUltiByDefender1 = True
        self.assertEqual(dummy.value(Role.challenger), 0)
        self.assertEqual(dummy.value(Role.defender1), 3)
        self.assertEqual(dummy.value(Role.defender2), -3)
        dummy.quietUltiFailed = True
        self.assertEqual(dummy.value(Role.challenger), 6)
        self.assertEqual(dummy.value(Role.defender1), -9)
        self.assertEqual(dummy.value(Role.defender2), 3)

        # piros ulti, kocsi, lo, ulti rekontra
        dummy.reset()
        dummy.redGame = True
        dummy.simple = True
        dummy.ulti = True
        dummy.ultiContraLevel = ContraLevels.recontra
        self.assertEqual(dummy.value(Role.challenger), -84)
        self.assertEqual(dummy.value(Role.defender1), 42)
        self.assertEqual(dummy.value(Role.defender2), 42)

        # sima parti, rekontra
        dummy.reset()
        dummy.simple = True
        dummy.simpleContraLevel = ContraLevels.recontra
        self.assertEqual(dummy.value(Role.challenger), -8)
        self.assertEqual(dummy.value(Role.defender1), 4)
        self.assertEqual(dummy.value(Role.defender2), 4)

        # szintelen, szintes durchmars
        dummy.reset()
        dummy.durchmars = True
        dummy.durchmarsSucceed = True
        self.assertEqual(dummy.value(Role.challenger), 12)
        self.assertEqual(dummy.value(Role.defender1), -6)
        self.assertEqual(dummy.value(Role.defender2), -6)
        dummy.colorless = True
        self.assertEqual(dummy.value(Role.challenger), 12)
        self.assertEqual(dummy.value(Role.defender1), -6)
        self.assertEqual(dummy.value(Role.defender2), -6)

        # szintelen redurchmars, piros durchmars, teritett durchmars
        dummy.reset()
        dummy.durchmars = True
        dummy.redGame = True
        dummy.durchmarsSucceed = True
        self.assertEqual(dummy.value(Role.challenger), 24)
        self.assertEqual(dummy.value(Role.defender1), -12)
        self.assertEqual(dummy.value(Role.defender2), -12)
        dummy.colorless = True
        self.assertEqual(dummy.value(Role.challenger), 24)
        self.assertEqual(dummy.value(Role.defender1), -12)
        self.assertEqual(dummy.value(Role.defender2), -12)
        dummy.colorless = False
        dummy.redGame = False
        dummy.openGame = True
        self.assertEqual(dummy.value(Role.challenger), 24)
        self.assertEqual(dummy.value(Role.defender1), -12)
        self.assertEqual(dummy.value(Role.defender2), -12)

        # betli, rebetli, teritett betli
        dummy.reset()
        dummy.colorless = True
        dummy.betli = True
        dummy.betliSucceed = True
        self.assertEqual(dummy.value(Role.challenger), 10)
        self.assertEqual(dummy.value(Role.defender1), -5)
        self.assertEqual(dummy.value(Role.defender2), -5)
        dummy.redGame = True
        self.assertEqual(dummy.value(Role.challenger), 20)
        self.assertEqual(dummy.value(Role.defender1), -10)
        self.assertEqual(dummy.value(Role.defender2), -10)
        dummy.redGame = False
        dummy.openGame = True
        self.assertEqual(dummy.value(Role.challenger), 40)
        self.assertEqual(dummy.value(Role.defender1), -20)
        self.assertEqual(dummy.value(Role.defender2), -20)
