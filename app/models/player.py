from db import db
from typing import Dict, Union, List

PlayerJSON = Dict[str, Union[int, str]]


class PlayerModel(db.Model):
    __tablename__ = "players"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200))

    def __init__(self, name: str):
        self.name = name

    @classmethod
    def find_by_name(cls, name: str) -> "PlayerModel":
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_id(cls, id: int) -> "PlayerModel":
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_all(cls) -> "List[PlayerModel]":
        return cls.query.order_by(cls.name).all()

    def save_to_db(self) -> None:
        db.session.add(self)
        db.session.commit()

    def remove_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()

    def json(self) -> PlayerJSON:
        return {"id": self.id, "name": self.name}
