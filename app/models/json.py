from flask.json import JSONEncoder
from models.game import GameModel


class UltiJSONEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, GameModel):
            return o.json()
        return super().default(o)
