
# WTF is UltiStat?
Ulti is a Hungarian card game. I love ulti and I play it a lot. My card playing buddies and I always wanted to have some long term game statistics, but we did not have an easy to access tool that could collect the results of the games. UltiStat is this tool. It is my first web application. Now, anywhere I play, I can pick my phone and store the result of a game. Later I can create all kinds of statistics. And this is awesome!

I am aware that this application is totally worthless for (nearly) anybody else. I started this project to learn Flask, SQLAlchemy, Bootstrap and to make practical experiences with Docker beyond playing with Hello World examples. I share this to help other fellow novice web application developers by showing my very simple application that uses all the mentioned technologies.

# Steps to deploy UltiStat on your own server
## Prerequisites
Before you can start with the deployment, following things should be insalled:
* Git
* Docker
* docker-compose

## Cloning the project
Clone the project into an arbitrary directory of your choice:
```
 git clone https://gitlab.com/vajdaz/ulti-stat.git
``` 

## Setting up secrets and web server configuration
Go into the project directory
```
 cp ulti-stat
```
Set the initial database root password in your ```docker-compose.yml``` and ```docker-compose-test.yml``` files.

Create and adjust your application configuration file:
```
 cp ulti.conf.tmpl ulti.conf
```
And edit the file with an editor of your choice.

* Change the secret key to a random value that only you know.
* Choose a password for the database root and it in the database uri string.
* Choose an initial password for the admin user of the web application.

## Setup port forwarding
If you run the server from your home network, you have to forward port 80 and port 443 to ports 8080 and 8443 of the computer, where the container will run.

## Configure nginx reverse proxy and the Let's Encrypt initialization script
Create the reverse proxy configuration file:
```
 cp nginx_conf/app.conf.tmpl nginx_conf/app.conf
```
Change the domain name in ```nginx_conf/app.conf``` and ```init-letsencrypt.sh``` to your domain name.

## Initialize SSL certificate and encryption key
Run ```init-letsencrypt.sh``` and follow the instructions.
If everything goes ok, you are up and running. Happy statistics!

However, after this initialization your services are not all running as desired. Especially the certbot service, which checks for certificate updates every 12 hours, is not running yet. Therefore you should stop all services and start them again with ```docker-compose```.

## Starting and stopping the service
You can start and stop the services by following commands:
```
 docker-compose up -d # starts all services "detached" in the background
 docker-compose down  # stopps all services and removes all containers
```

If something is not working correctly, the logs can be displayed by following command:
```
 docker-compose logs -f
```

## Persistence
Following Docker volumes are automatically created during the deployment process:
* ```ulti_db_test``` and ```ulti_db_productive``` contains the MySQL database files (depending on which docker compose file is used)
* ```ulti_certbot_conf``` contains the Let's Encrypt certificates, keys and configuration data
* ```ulti_certbot_www``` is used to publish the http challenge file when certificates are requested or renewed

# Fun with kubernetes

If you want to play with kubernetes, you may try to deploy this webapp onto a local development cluster. I wrote [some instrcutions](k8s/deploy-on-kubernetes-with-kind.md) how to do it.