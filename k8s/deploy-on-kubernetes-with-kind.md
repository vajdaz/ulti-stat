# How to deploy this on a playground kubernetes

After having the web application runnung under docker and with docker compose, the next thing I wanted to learn is how to deploy the system it in a kubernetes cluster. By doing this I learned following kubernetes concepts:

* Using a deployment and a service to deploy a web service
* Using a stateful set and a headless service to deploy a database service
* Pod definitions inside of the deployment and the stateful set
* Injecting configuration information into pods by mounting secrets as files into the containers and by using secrets as environment variables
* Definition of a readyness probe in the pod where the web application runs
* Configuration of an ingress to make the web application available for the users
* Using a volume to persist database files
* Defining a security policy to control network traffic between pods

Try also to play with kubernetes, it's fun! But where to get a cluster full functional kubernetes cluster to play with? I tried two alternatives:

* [Minikube](https://kubernetes.io/docs/tutorials/hello-minikube)
* [kind](https://kind.sigs.k8s.io) (Kubernetes IN Docker)

Both come from the kubernetes community, and both are great. I chose kind because I am developing under Linux, and running the cluster nodes as docker containers is more efficient than running them on VMs. Another reason is, that kind seemed to be more like a real kubernetes installation: you can easily configure multiple nodes, you can run several cluster at the same time, for example. However, Minikube is maybe a little more beginner friendly. Research on your own and choose wisely. My instructions are only tested with kind. The deployment of the Flask app and the database should be the same. There surely are differences in the installation and handling of the ingress controler and mounting host drives into cluster nodes. So if you choose Minikube, you will have to do some research and do those things a little bit differently.

## Install kind

kind is a tool for running local Kubernetes clusters. Here you can find the [installation instructions](https://kind.sigs.k8s.io/docs/user/quick-start/#installation).

If you are done with the installation, go on. All steps that follow, should be executed in the ```k8s``` directory of the working tree of this Git repo.

## Create kind cluster

Create a cluster. Use the configuration provided, because otherwise the ingress will not start, and ports will not be exposed. Therefore, you will not able to connect to the application.
```
kind create cluster --config=kind-cluster.conf
```
The ```.conf``` file is actually a YAML file, too. But later, when we deploy all the other yaml files in this directory with ```kubectl```, we would get an error, because ```kubectl``` would try to apply the cluster configuration, too, but that is not a valid kubernetes resource configuration. Therefore we name the file differently.

## Install nginx ingress controller

A configuration is kindly provided by the kind community:
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
```

## Finally deploy all the stuff

Run following command to deploy all the stuff necessary to have a working webapplication. You may compare the configuration in all the YAML files with the ```docker-compose.yml``` in the project root directory. Here we do not start a certbot, and we do not have a reverse proxy. Later is replaced by the ingress.
```
kubectl apply -f .
```

If you happen to see following error message...
```
Error from server (InternalError): error when creating "ultistat-ingress.yml": Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": Post "https://ingress-nginx-controller-admission.ingress-nginx.svc:443/networking/v1beta1/ingresses?timeout=10s": dial tcp xx.xx.xx.xx:443: connect: connection refused
```
...then the ingress controller did not start yet. Wait a little and try again.

After evertyhing has been deployed succesfully, you can inpect the pods beeing created and started by regulary executing
```
kubectl get pods
```
It takes one or two minutes for the images to be downloaded and the containers to be started.

## And voilá...

Open a browser and enter ```https://localhost``` into the address bar.

You will see a security warning, because the TLS certificate is a self signed one. Accept the "risks" and there you go.

## Small isse with network policies

After all pods are up and running, and the ingress is also correctly configured, you may see following error message in the browser in the first few minutes:

>503 Service Temporarily Unavailable

The reason for this seems to be, that it takes a quite long time for the network policy configuration to become active. During this time the communication between the Flask app and the database is blocked. A readyness probe is defined for the web app pod, that checks the connectivity, and reports the pod as ready if it can see port 3306 of the database pod. If this long waiting time bothers you, don't apply ```network-policy.yml```.

## Persistence

The database files are persisted by mounting the ```k8s/volume``` project subdirectory into the kind cluster node container at the mount point ```/data``` (see ```kind-cluster.conf```), and then mounting the ```/data``` directory of the node into the database pod at the mount point ```/var/lib/mysql``` (see ```db-deployment.yml```).