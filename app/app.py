from flask.helpers import send_file
from models.game import ContraLevels
from models.json import UltiJSONEncoder
from db import db
from flask import Flask, current_app, redirect, url_for
from auth import (
    LoginWithGoogleView,
    bp as auth_bp,
    login_manager as lm,
    LoginView,
    LogoutView,
    LoginWithGoogleView,
    GoogleCallbackView,
)
from ulti import (
    bp as ulti_bp,
    PlayerListView,
    PlayerApi,
    LastPlayersApi,
    GamesListView,
    ResultSummaryView,
    GameApi,
)
import os
from werkzeug.middleware.proxy_fix import ProxyFix

app = Flask(
    __name__,
    template_folder=os.path.abspath("templates"),
    static_folder=os.path.abspath("static"),
)

app.secret_key = "nobody-knows"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get(
    "SQLALCHEMY_DATABASE_URI", "sqlite:///data.db"
)
app.config["ULTI_DEFAULT_ADMIN_PWD"] = os.environ.get("ULTI_DEFAULT_ADMIN_PWD", "admin")
app.config["PROPAGATE_EXCEPTIONS"] = True
if "ULTI_FLASK_CONFIG" in os.environ:
    app.config.from_envvar("ULTI_FLASK_CONFIG")

app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1)

app.json_encoder = UltiJSONEncoder

db.init_app(app)
lm.init_app(app)

app.register_blueprint(auth_bp)
app.register_blueprint(ulti_bp)

app.add_url_rule("/auth/login", view_func=LoginView.as_view("auth.login"))
app.add_url_rule("/auth/glogin", view_func=LoginWithGoogleView.as_view("auth.glogin"))
app.add_url_rule(
    "/auth/glogin/callback", view_func=GoogleCallbackView.as_view("auth.callback")
)
app.add_url_rule("/auth/logout", view_func=LogoutView.as_view("auth.logout"))
app.add_url_rule("/ulti/players", view_func=PlayerListView.as_view("ulti.players"))
app.add_url_rule("/ulti/games", view_func=GamesListView.as_view("ulti.games"))
app.add_url_rule(
    "/ulti/result-summary", view_func=ResultSummaryView.as_view("ulti.result-summary")
)
app.add_url_rule(
    "/api/ulti/player/<int:id>", view_func=PlayerApi.as_view("ulti.player_api")
)
app.add_url_rule(
    "/api/ulti/game/<int:id>",
    view_func=GameApi.as_view("ulti.game_api"),
)
app.add_url_rule(
    "/ulti/last-players", view_func=LastPlayersApi.as_view("ulti.last_players_api")
)


@app.before_first_request
def create_tables():
    from models.user import UserModel
    from models.player import PlayerModel
    from models.game import GameModel
    from datetime import time
    import sqlalchemy

    if app.config["SQLALCHEMY_DATABASE_URI"].startswith("mysql"):
        engine = sqlalchemy.create_engine(
            app.config["SQLALCHEMY_DATABASE_URI"].replace("/ultistat", "")
        )
        engine.execute("CREATE SCHEMA IF NOT EXISTS `ultistat`;")

    db.create_all()
    if not UserModel.query.all():
        UserModel.save_to_db(
            UserModel("admin", current_app.config["ULTI_DEFAULT_ADMIN_PWD"])
        )
    if not PlayerModel.query.all():
        UserModel.save_to_db(PlayerModel("Játékos 1"))
        UserModel.save_to_db(PlayerModel("Játékos 2"))
        UserModel.save_to_db(PlayerModel("Játékos 3"))
    if not GameModel.query.all():
        players = PlayerModel.find_all()
        if len(players) >= 3:
            exampleGame = GameModel(players[0], players[1], players[2])
            exampleGame.time = time(hour=16, minute=42)
            exampleGame.betli = True
            exampleGame.colorless = True
            exampleGame.redGame = True
            GameModel.save_to_db(exampleGame)
            exampleGame = GameModel(players[1], players[2], players[0])
            exampleGame.fourAsses = True
            exampleGame.simple = True
            exampleGame.fourAssesSucceed = True
            exampleGame.time = time(hour=16, minute=55)
            GameModel.save_to_db(exampleGame)
            exampleGame = GameModel(players[2], players[0], players[1])
            exampleGame.time = time(hour=17, minute=5)
            exampleGame.simple = True
            exampleGame.simpleSucceed = True
            exampleGame.simpleContraLevel = ContraLevels.contra
            GameModel.save_to_db(exampleGame)


@app.route("/")
def root():
    return redirect(url_for("ulti.games"))


@app.route("/favicon.ico")
def favicon():
    return send_file(app.static_folder + "/images/favicon.ico", mimetype="image/x-icon")


if __name__ == "__main__":
    app.run()
